Source: dnapi
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: science
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-setuptools,
               python3-all
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/dnapi
Vcs-Git: https://salsa.debian.org/med-team/dnapi.git
Homepage: https://github.com/jnktsj/DNApi/
Rules-Requires-Root: no

Package: dnapi
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-dnapilib
Description: adapter prediction for small RNA sequencing - utils
 This package provides de novo adapter prediction (iterative) algorithm
 for small RNA sequencing data.
 .
 DNApi can predict most 3' adapters correctly with the default
 parameters. You can tweak the parameters and can run different
 prediction modes.

Package: python3-dnapilib
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${misc:Depends}
Description: adapter prediction for small RNA sequencing - library
 This Python3 module provides de novo adapter prediction (iterative)
 algorithm for small RNA sequencing data.
 .
 DNApi can predict most 3' adapters correctly with the default
 parameters. You can tweak the parameters and can run different
 prediction modes.
