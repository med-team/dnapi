from dnapilib.apred import adapter_prediction
from dnapilib.apred import iterative_adapter_prediction

def test_functional():
    iterative_result = iterative_adapter_prediction("good.fq", [1.2, 1.3, 1.4], [9, 11], 50000)
    single_result = adapter_prediction("good.fq", 1.4, 9, 50000)


    assert(iterative_result[0][0] == 'TGGAATTCTCGG')
    assert(single_result[0][0] == 'TGGAATTCTCGGGTGCCAAGGAACTCC')

    iterative_result = iterative_adapter_prediction("processed.fq", [1.2, 1.3, 1.4], [9, 11], 50000)
    single_result = adapter_prediction("processed.fq", 1.4, 9, 50000)

    assert([x[0] for x in iterative_result] == ['TCTGCCCAGTGCTCTG', 'TAATACTGCCTG', 'TGGCAGTGTCTT'])
    assert([x[0] for x in single_result] == ['TAATACTGCCTGGTAATGATG', 'TGGCAGTGTCTTAGCTGGTTG', 'CTGCCCAGTGCTCTGAA'])
